BASELIB=baselib
XA=xa -vC -I$(BASELIB)

all: clean qr-encoder.nes

clean:
	rm -f *.bin qr-encoder.nes

qr-encoder.nes: chr-rom.bin prg-rom.bin
	$(XA) $(BASELIB)/ines.asm -o qr-encoder.nes

chr-rom.bin:
	$(XA) $(BASELIB)/chr-rom.asm -o chr-rom.bin

prg-rom.bin:
	$(XA) $(BASELIB)/prg-rom.asm -o prg-rom.bin
