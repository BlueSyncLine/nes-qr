; A simple iNES header for NROM-128.

; Start at zero.
* = 0

; iNES header.
header: .byt "NES", $1A ; Magic value
prg16:  .byt 1          ; One 16 KB block for PRG ROM.
chr8:   .byt 1          ; One 8 KB block for CHR ROM.
flags6: .byt 1          ; Vertical mirroring.
flags7: .byt 0          ; Unused.
flags8: .byt 0          ; Unused.
flags9: .byt 0          ; Unused.
flagsA: .byt 0          ; Unused.

; Padding.
.dsb 16 - *, 0

; PRG and CHR ROM data.
.bin 0, 16384, "prg-rom.bin"
.bin 0, 8192, "chr-rom.bin"
