; Hardware registers.
#include "./registers.asm"

; Macro to set the PPU address.
#define PPU_set_address(addr) \
    bit PPUSTATUS:\
    lda #(addr) >> 8:\
    sta PPUADDR:\
    lda #(addr) & $ff:\
    sta PPUADDR

; Write a 16-bit word, LSB first, to a memory address.
#define store_word(addr,word) \
    lda #(word) & $ff:\
    sta addr:\
    lda #(word) >> 8:\
    sta addr+1

; Variables.
NMI_COUNT = $00
TEMP_A    = $01
TEMP_B    = $02

; The user zero-page memory starts from $10.
USER_ZPG = $10

; The CPU sees the PRG ROM at $8000.
* = $8000

; Default NMI handler.
default_NMI:
    inc NMI_COUNT
    rti

; Default IRQ handler.
default_IRQ:
    rti

; Initialization code (modified from nesdev.com).
RESET:
.(
    ; Note - there's no need to disable the APU; APUSTATUS is zeroed on reset.

    ; No IRQs.
    sei

    ; Disable the decimal mode.
    cld

    ; The stack is at $FF.
    ldx #$ff
    txs

    ; Wait for two vblanks to make sure it's safe to write to PPU registers.
    bit PPUSTATUS
    jsr wait_vblank_unsafe
    jsr wait_vblank_unsafe

    ; Set PPU parameters.
    ; - base nametable address $2000.
    ; - VRAM address increment 1.
    ; - sprite CHR address $0000.
    ; - background CHR address $0000.
    ; - 8x8 sprites
    ; - PPU master
    ; - NMI allowed
    lda #PPUCTRL_NMI
    sta PPUCTRL

    ; Disable the APU frame counter IRQ.
    lda #APUFRAME_NOINT
    sta APUFRAME

    ; No rendering.
    lda #$00
    sta PPUMASK

    ; No DMC IRQs.
    sta APUDMC_FLAGS

    ; Reset the memory into a known state.
    ldx #$00
clear_ram:
    sta $000, x
    sta $100, x
    sta $200, x
    sta $300, x
    sta $400, x
    sta $500, x
    sta $600, x
    sta $700, x
    inx
    bne clear_ram

    ; Clear the first nametable.
    jsr clear_nametable

    ; Load a meaningful palette.
    PPU_set_address($3f00)
    lda #$0f
    sta PPUDATA
    lda #$10
    sta PPUDATA
    lda #$00
    sta PPUDATA
    lda #$2a
    sta PPUDATA

    ; Reset scrolling.
    jsr reset_scroll

    ; Done.
    jmp main

; Initial vblank wait code.
wait_vblank_unsafe:
    bit PPUSTATUS
    bpl wait_vblank_unsafe
    rts
.)

; Waits for a vertical blanking.
; Can be used after the PPU has initialized.
; Sourced from nesdev.com.
; A is destroyed.
wait_vblank:
.(
    ; Load the current NMI counter.
    lda NMI_COUNT
loop:
    ; Has it changed?
    cmp NMI_COUNT

    ; No, try again.
    beq loop

    ; Yes, return.
    rts
.)

; Enables rendering.
; Destroys A.
render_enable:
    jsr wait_vblank
    lda #PPUMASK_BG
    sta PPUMASK
    rts

; Disables rendering.
; Destroys A.
render_disable:
    jsr wait_vblank
    lda #$00
    sta PPUMASK
    rts

; Resets the PPU scrolling registers.
; Destroys A.
reset_scroll:
    ; Reset the PPU address latch.
    bit PPUSTATUS

    ; Reset the scrolling coordinates.
    lda #$00
    sta PPUSCROLL
    sta PPUSCROLL

    ; The address can switch into a different nametable; reset it.
    sta PPUADDR
    sta PPUADDR
    rts

; Clears the first nametable.
; Destroys A, X and Y.
clear_nametable:
.(
    ; We're going to zero out 1024 bytes at PPU address $2000.
    PPU_set_address($2000)

    ; Zero bytes.
    lda #$00

    ; Four times 256.
    ldy #$4

next256:
    ; Start at zero.
    ldx #$00
byte:
    ; Zero out a byte.
    sta PPUDATA

    ; Increment X.
    inx

    ; If there wasn't an overflow, repeat.
    bne byte

    ; Next 256 bytes?
    dey
    bne next256

    ; No, return.
    rts
.)

; Go to the nametable tile at X, Y.
; Destroys A and TEMP_A.
goto_tile:
    ; Reset the address latch.
    bit PPUSTATUS

    ; Eight rows are 256 bytes.
    ; Divide and use the result to compute the MSB of the PPU address.
    tya
    lsr
    lsr
    lsr
    clc

    ; Add the original MSB.
    adc #$20

    ; Write the MSB.
    sta PPUADDR

    ; A = Y*32 + X
    tya
    asl
    asl
    asl
    asl
    asl

    ; Add X.
    stx TEMP_A
    clc
    adc TEMP_A

    ; Write the LSB.
    sta PPUADDR

    ; Done.
    rts

; Writes a string with an address stored at STRING_ADDRESS to the screen.
; PPUADDR is assumed to point to a starting tile.
; Destroys A.
STRING_ADDRESS = TEMP_A
draw_string:
.(
    ; Preserve Y.
    tya
    pha

    ; Y is used for addressing.
    ldy #$00

loop:
    ; Get the next byte.
    lda (TEMP_A), Y

    ; Zero?
    beq done

    ; Write PPU data.
    sta PPUDATA

    ; Loop.
    iny
    jmp loop

done:
    ; Reset the PPUSCROLL registers which have been modified by the above.
    jsr reset_scroll

    ; Restore Y, return.
    pla
    tay
    rts
.)

; Clever controller reading code, adapted from nesdev.com.
; The value is stored in TEMP_A. Register A is destroyed.
JOYBTN_A       = $80
JOYBTN_B       = $40
JOYBTN_SELECT  = $20
JOYBTN_START   = $10
JOYBTN_UP      = $08
JOYBTN_DOWN    = $04
JOYBTN_LEFT    = $02
JOYBTN_RIGHT   = $01
read_joypad:
.(
    ; Strobe controller A.
    lda #$01
    sta JOYPADA

    ; Will set carry when shifted out.
    sta TEMP_A

    ; Stop reloading.
    lda #$00
    sta JOYPADA

loop:
    ; Shift in.
    lda JOYPADA

    ; Bit 0 into the carry.
    lsr

    ; Shift the temporary value.
    rol TEMP_A

    ; As long as we didn't shift out the initial 1 bit,
    ; continue looping.
    bcc loop

    ; Return.
    rts
.)

; Halt.
halt:
    jmp halt


; Waits for X vblanks to occur.
; X is destroyed.
wait_vblanks:
.(
loop:
    jsr wait_vblank
    dex
    bne loop

    rts
.)

#include "main.asm"

; If the main file did not redefine the NMI handler...
#ifldef NMI
#else
NMI = default_NMI
#endif

; Or the IRQ handler.
#ifldef IRQ
#else
IRQ = default_IRQ
#endif

; In NROM-128 the memory is mirrored, so the vectors are at $bffa.
.dsb $bffa - *, 0
.word NMI
.word RESET
.word IRQ

