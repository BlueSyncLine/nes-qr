; This program uses a hardcoded version 1 QR symbol with L error correction.
; The mask is also fixed.


; Variable addresses.
; The target 19 message bytes.
MESSAGE_BYTES = USER_ZPG

; The encoded message (26 bytes).
ENCODED_MESSAGE = MESSAGE_BYTES + 19

; The byte pointer that's used when the code is drawn.
BUFFER_INDEX = ENCODED_MESSAGE + 26

; A counter value used during drawing with routines that affect TEMP_A/TEMP_B.
DRAWING_COUNTER = BUFFER_INDEX + 1

; The input text.
INPUT_TEXT = DRAWING_COUNTER + 1

; The input text is always 17 bytes long.
INPUT_TEXT_LENGTH = 17

; The cursor position variable.
CURSOR_POSITION = INPUT_TEXT + INPUT_TEXT_LENGTH

; The stripped length of the input text.
INPUT_LENGTH_STRIP = CURSOR_POSITION + 1

; X and Y offsets for the text and for drawing the QR code.
QR_X_OFFSET = 5
QR_Y_OFFSET = 4
INTRO_TEXT_X = 8
INTRO_TEXT_Y = 4
INPUT_FIELD_X = 6
INPUT_FIELD_Y = 6
DIRECTIONS_X = 2
DIRECTIONS_Y = 10
QUIT_MESSAGE_X = 3
QUIT_MESSAGE_Y = 27

; Joypad input delay (vblanks).
JOYPAD_DELAY = 5

; Pre-calculated format data (ECC L, mask 0).
; MSB-first.
QR_FORMAT_A = $47
QR_FORMAT_B = $ac

; Reed-Solomon parameters.
; The irreducible polynomial of the field used.
GF_POLY = $11d

; The generator polynomial for 7 ECC bytes.
gf_generator_poly:
    .byt 1, 127, 122, 154, 164, 11, 68, 117

; Galois-field multiplication.
; TEMP_A is multiplied by TEMP_B, the result is stored in A.
; TEMP_A, TEMP_B are destroyed.
gf_mul:
.(
    ; A is our multiplication accumulator.
    lda #$00
    pha

loop:
    ; Test for lower bit of TEMP_B.
    ; If set, add the current value of TEMP_A to the accumulator.
    lda TEMP_B

    ; TEMP_B is zero, we're done.
    beq done

    ; Test bit.
    and #$01
    beq skip_add

    ; Add TEMP_A to the accumulator.
    pla
    eor TEMP_A
    pha

skip_add:
    ; Shift TEMP_B, going to the next bit.
    lda TEMP_B
    lsr
    sta TEMP_B

    ; Multiply TEMP_A by 2, reduce if needed.
    lda TEMP_A
    asl
    bcc no_modulo

    ; Reduce.
    eor #GF_POLY&$ff

no_modulo:
    sta TEMP_A
    jmp loop

done:
    pla
    rts
.)


; Encode MESSAGE_BYTES into ENCODED_MESSAGE.
rs_encode:
.(
    ; Copy the message bytes.
    ldx #0

copy_loop:
    lda MESSAGE_BYTES, x
    sta ENCODED_MESSAGE, x
    inx
    cpx #19
    bne copy_loop

    ; Zero padding.
    lda #$00
padding_loop:
    sta ENCODED_MESSAGE, x
    inx
    cpx #27
    bne padding_loop

    ; ENCODED_MESSAGE now contains the message bytes and the padding.
    ; Perform polynomial reduction.

    ; Start from the first byte.
    ldx #0
division_loop:
    ldy #0
    lda ENCODED_MESSAGE, x

    ; Preserve the initial high byte.
    pha
inner_loop:
    ; Restore the high byte; multiply it by the generator's Yth member.
    pla
    pha
    sta TEMP_A
    lda gf_generator_poly, y
    sta TEMP_B

    ; A = TEMP_A gmul TEMP_B
    jsr gf_mul

    ; Preserve X and A.
    stx TEMP_A
    sta TEMP_B

    ; X = Y + X.
    tya
    clc
    adc TEMP_A
    tax

    ; XOR the value of A into the polynomial.
    lda ENCODED_MESSAGE, x
    eor TEMP_B
    sta ENCODED_MESSAGE, x

    ; Restore X.
    ldx TEMP_A

    ; Do this for every byte of the generator polynomial.
    iny
    cpy #8
    bne inner_loop

    ; Remove the saved value from the top of the stack.
    pla

    ; Iterate the outer loop.
    inx
    cpx #19
    bne division_loop

    ; Done, copy the message over again.
    ldx #0

second_copy_loop:
    lda MESSAGE_BYTES, x
    sta ENCODED_MESSAGE, x
    inx
    cpx #19
    bne second_copy_loop
    rts
.)

; Wraps goto_tile for drawing QR pixels.
; Preserves X and Y.
; A is destroyed.
put_pixel:
    ; X coordinate, preserve the original and add an offset.
    txa
    pha
    clc
    adc #QR_X_OFFSET
    tax

    ; The same with Y.
    tya
    pha
    clc
    adc #QR_Y_OFFSET
    tay

    ; Call goto_tile.
    jsr goto_tile

    ; Place a $7f character, hopefully a box.
    lda #$7f
    sta PPUDATA

    ; Reset scrolling.
    jsr reset_scroll

    ; Restore the coordinates.
    pla
    tay
    pla
    tax
    rts

; Put a data bit (MSB of A), XOR against the default mask.
put_data:
.(
    ; Preserve A.
    pha

    ; Shift the MSB into the LSB.
    lsr
    lsr
    lsr
    lsr
    lsr
    lsr
    lsr

    ; Apply the mask - XOR with the coordinates, get the lowest bit.
    stx TEMP_A
    eor TEMP_A
    sty TEMP_A
    eor TEMP_A
    and #$01

    ; Skip?
    bne skip_draw

    ; Draw pixel.
    jsr put_pixel
skip_draw:
    ; Restore A.
    pla
    rts
.)

; Put a raw data bit (MSB of A), without masking.
put_data_raw:
.(
    ; Preserve A.
    pha

    ; MSB into LSB.
    lsr
    lsr
    lsr
    lsr
    lsr
    lsr
    lsr

    ; Zero? Skip.
    beq skip_draw

    ; Draw pixel
    jsr put_pixel
skip_draw:
    ; Restore A.
    pla
    rts
.)

; Draws the QR base pattern.
; X and Y are destroyed.
qr_draw_base:
.(
    ; First square (at 0, 0).
    ldx #0
    ldy #0

line1:
    jsr put_pixel
    inx
    cpx #7
    bne line1
    dex
line2:
    jsr put_pixel
    iny
    cpy #7
    bne line2
    dey
line3:
    dex
    jsr put_pixel
    cpx #0
    bne line3
line4:
    dey
    jsr put_pixel
    cpy #0
    bne line4

    ; Second square (at 14, 0).
    ldx #14
    ldy #0

line5:
    jsr put_pixel
    inx
    cpx #21
    bne line5
    dex
line6:
    jsr put_pixel
    iny
    cpy #7
    bne line6
    dey
line7:
    dex
    jsr put_pixel
    cpx #14
    bne line7
line8:
    dey
    jsr put_pixel
    cpy #0
    bne line8

    ; Third square (at 0, 14).
    ldx #0
    ldy #14

line9:
    jsr put_pixel
    inx
    cpx #7
    bne line9
    dex
line10:
    jsr put_pixel
    iny
    cpy #21
    bne line10
    dey
line11:
    dex
    jsr put_pixel
    cpx #0
    bne line11
line12:
    dey
    jsr put_pixel
    cpy #14
    bne line12

    ; First 3x3 box (at 2, 2).
    ldx #2
    ldy #2
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #2
    ldy #3
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #2
    ldy #4
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel

    ; Second 3x3 box (at 16, 2).
    ldx #16
    ldy #2
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #16
    ldy #3
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #16
    ldy #4
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel

    ; Third 3x3 box (at 2, 16).
    ldx #2
    ldy #16
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #2
    ldy #17
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel
    ldx #2
    ldy #18
    jsr put_pixel
    inx
    jsr put_pixel
    inx
    jsr put_pixel

    ; Draw the timing pattern between the boxes.
    ; Horizontal
    ldx #8
    ldy #6
    jsr put_pixel
    inx
    inx
    jsr put_pixel
    inx
    inx
    jsr put_pixel

    ; Vertical
    ldx #6
    ldy #8
    jsr put_pixel
    iny
    iny
    jsr put_pixel
    iny
    iny
    jsr put_pixel

    ; Extra timing pixel.
    ldx #8
    ldy #13
    jsr put_pixel

    ; Draw the format information.
    ; Horizontal
    ldx #0
    ldy #8
    lda #QR_FORMAT_A

hformat_loop1:
    jsr put_data_raw
    asl
    inx
    cpx #6
    bne hformat_loop1

    inx
    jsr put_data_raw
    asl

    inx
    inx
    inx
    inx
    inx
    inx

    jsr put_data_raw
    inx
    lda #QR_FORMAT_B
hformat_loop2:
    jsr put_data_raw
    asl
    inx
    cpx #21
    bne hformat_loop2

    ; Vertical
    ldx #8
    ldy #20
    lda #QR_FORMAT_A
vformat_loop1:
    jsr put_data_raw
    asl
    dey
    cpy #13
    bne vformat_loop1

    dey
    dey
    dey
    dey
    dey
    jsr put_data_raw
    dey

    lda #QR_FORMAT_B
    jsr put_data_raw
    asl
    dey
    dey
vformat_loop2:
    jsr put_data_raw
    asl
    cpy #0
    beq done
    dey
    jmp vformat_loop2

done:
    ; Done.
    rts
.)


; Draw a part of a zigzag data block (up).
data_row_up:
    jsr put_data
    asl
    dex
    jsr put_data
    asl
    inx
    dey
    rts


; Draw a part of a zigzag data block (down).
data_row_down:
    jsr put_data
    asl
    dex
    jsr put_data
    asl
    inx
    iny
    rts


; Draw an upwards zigzag data block.
; A is the data byte, X and Y are modified appropriately.
data_block_up:
    jsr data_row_up
    jsr data_row_up
    jsr data_row_up
    jsr data_row_up
    rts


; Draw an downwards zigzag data block.
; A is the data byte, X and Y are modified appropriately.
data_block_down:
    jsr data_row_down
    jsr data_row_down
    jsr data_row_down
    jsr data_row_down
    rts

; Draw an upwards zigzag data block with a gap.
; A is the data byte, X and Y are modified appropriately.
data_block_gap_up:
    jsr data_row_up
    jsr data_row_up
    dey
    jsr data_row_up
    jsr data_row_up
    rts


; Draw an downwards zigzag data block with a gap.
; A is the data byte, X and Y are modified appropriately.
data_block_gap_down:
    jsr data_row_down
    jsr data_row_down
    iny
    jsr data_row_down
    jsr data_row_down
    rts

; Resets the buffer index.
; A is destroyed.
buffer_reset:
    lda #$00
    sta BUFFER_INDEX
    rts

; Pulls the next byte from the ENCODED_MESSAGE buffer.
; A contains the result.
; TEMP_A is destroyed.
buffer_next:
    ; Preserve X.
    stx TEMP_A

    ; Retrieve the value, increment index.
    ldx BUFFER_INDEX
    lda ENCODED_MESSAGE, x
    inx
    stx BUFFER_INDEX

    ; Restore X.
    ldx TEMP_A
    rts

; Populates the QR code with data from ENCODED_MESSAGE.
; Destroys the registers, TEMP_A and TEMP_B.
qr_draw_data:
.(
    ; From the start of the buffer.
    jsr buffer_reset

    ; Starting from the bottom left corner...
    ldx #20
    ldy #20

    ; Draw two "towers" of data blocks.
    lda #2
    sta DRAWING_COUNTER

vertical_towers:
    jsr buffer_next
    jsr data_block_up
    jsr buffer_next
    jsr data_block_up
    jsr buffer_next
    jsr data_block_up
    iny
    dex
    dex
    jsr buffer_next
    jsr data_block_down
    jsr buffer_next
    jsr data_block_down
    jsr buffer_next
    jsr data_block_down
    dey
    dex
    dex

    ; Repeat.
    dec DRAWING_COUNTER
    bne vertical_towers

    ; And up again...
    jsr buffer_next
    jsr data_block_up
    jsr buffer_next
    jsr data_block_up
    jsr buffer_next
    jsr data_block_up
    jsr buffer_next
    jsr data_block_gap_up
    jsr buffer_next
    jsr data_block_up
    iny
    dex
    dex

    ; And down...
    jsr buffer_next
    jsr data_block_down
    jsr buffer_next
    jsr data_block_gap_down
    jsr buffer_next
    jsr data_block_down
    jsr buffer_next
    jsr data_block_down
    jsr buffer_next
    jsr data_block_down

    ; And finally, the narrow section.
    ldx #8
    ldy #12
    jsr buffer_next
    jsr data_block_up

    ; Extra gap for the timing pattern.
    dex

    iny
    dex
    dex
    jsr buffer_next
    jsr data_block_down
    dey
    dex
    dex
    jsr buffer_next
    jsr data_block_up
    iny
    dex
    dex
    jsr buffer_next
    jsr data_block_down

    ; Done.
    rts
.)

; Copies bytes from INPUT_TEXT into MESSAGE_BYTES.
; The padding is also added, for a total of 19 encoded bytes.
; Destroys A, X, Y and INPUT_TEXT.
pack_text:
.(
    ldx #0
    ldy #0

    ; Get the length of the stripped input text.
    lda INPUT_LENGTH_STRIP
    sta TEMP_A

    ; Encoding type and the high nibble of the length.
    lda #$04
    asl TEMP_A
    rol
    asl TEMP_A
    rol
    asl TEMP_A
    rol
    asl TEMP_A
    rol
    sta MESSAGE_BYTES, y
    iny

    ; Load the length again.
    lda INPUT_LENGTH_STRIP

    ; Immediately terminate if the length is zero.
    beq terminate

    ; Get the low nibble.
    and #$0f
loop:
    ; Shift in four bits from the input text.
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol

    ; Store, increment Y.
    sta MESSAGE_BYTES, y
    iny

    ; Shift in more bits.
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol
    asl INPUT_TEXT, x
    rol

    ; Done for this byte.
    inx

    ; More?
    cpx INPUT_LENGTH_STRIP
    bne loop

    ; Pack the remaining bits with a 0000 terminator.
terminate:
    asl
    asl
    asl
    asl
    sta MESSAGE_BYTES, y
    iny

    ; Add the $ec/$11 padding if needed.
    lda #$ec
padding:
    ; Have we reached the end of the message?
    cpy #19
    beq done

    ; No, add a padding byte.
    sta MESSAGE_BYTES, y
    iny

    ; Hack, XOR'ing with $fd will make A alternate $ec/$11.
    eor #$fd
    jmp padding

done:
    ; Done.
    rts
.)

; Sets a palette appropriate for drawing QR codes.
; Destroys A.
qr_palette:
    PPU_set_address($3f00)
    lda #$10
    sta PPUDATA
    lda #$0f
    sta PPUDATA

    ; Reset the scrolling registers, as we wrote into PPUDATA.
    jsr reset_scroll
    rts

; Returns the palette to the default configuration.
default_palette:
    PPU_set_address($3f00)
    lda #$0f
    sta PPUDATA
    lda #$10
    sta PPUDATA

    ; Reset the scrolling registers, as we wrote into PPUDATA.
    jsr reset_scroll
    rts

; Encodes and draws a QR code from INPUT_TEXT.
; The registers, TEMP_A and TEMP_B are destroyed.
qr_full:
    jsr pack_text
    jsr rs_encode
    jsr render_disable
    jsr clear_nametable
    jsr qr_palette
    jsr qr_draw_base
    jsr qr_draw_data

    ; Print the "Press any key to quit" message.
    ldx #QUIT_MESSAGE_X
    ldy #QUIT_MESSAGE_Y
    jsr goto_tile
    store_word(TEMP_A, quit_message)
    jsr draw_string

    jsr render_enable
    rts

main:
    ; Request user input.
    jsr input_screen

    ; Draw the code.
    jsr qr_full

    ; Wait for the keys to be released.
wait_release:
    ldx #JOYPAD_DELAY
    jsr wait_vblanks

    jsr read_joypad
    lda #$ff
    bit TEMP_A
    bne wait_release

    ; Wait for a keypress.
wait_keypress:
    ldx #JOYPAD_DELAY
    jsr wait_vblanks

    jsr read_joypad
    lda #$ff
    bit TEMP_A
    beq wait_keypress

    ; Wait for the keys to be released once again.
wait_release2:
    ldx #JOYPAD_DELAY
    jsr wait_vblanks

    jsr read_joypad
    lda #$ff
    bit TEMP_A
    bne wait_release2

    ; Start over.
    jmp main


; Displays a menu, prompting the user to fill INPUT_TEXT.
input_screen:
.(
    ; Copy over the test message.
    ldx #0
copy_loop:
    lda test_text, x
    sta INPUT_TEXT, x
    inx
    cpx #INPUT_TEXT_LENGTH
    bne copy_loop

    ; Set the cursor position.
    lda #0
    sta CURSOR_POSITION

    ; Disable rendering.
    jsr render_disable

    ; Clear the nametable.
    jsr clear_nametable

    ; Load the default palette.
    jsr default_palette

    ; Display the intro text and directions.
    ldx #INTRO_TEXT_X
    ldy #INTRO_TEXT_Y
    jsr goto_tile
    store_word(TEMP_A, intro_text)
    jsr draw_string

    ldx #DIRECTIONS_X
    ldy #DIRECTIONS_Y
    jsr goto_tile
    store_word(TEMP_A, directions_1)
    jsr draw_string
    ldx #DIRECTIONS_X
    ldy #DIRECTIONS_Y+2
    jsr goto_tile
    store_word(TEMP_A, directions_2)
    jsr draw_string
    ldx #DIRECTIONS_X
    ldy #DIRECTIONS_Y+4
    jsr goto_tile
    store_word(TEMP_A, directions_3)
    jsr draw_string
    ldx #DIRECTIONS_X
    ldy #DIRECTIONS_Y+6
    jsr goto_tile
    store_word(TEMP_A, directions_4)
    jsr draw_string
    ldx #DIRECTIONS_X
    ldy #DIRECTIONS_Y+8
    jsr goto_tile
    store_word(TEMP_A, directions_5)
    jsr draw_string

    ; Turn on rendering.
    jsr render_enable

    ; Input loop.
input_loop:
    ; We can only draw during vertical blanking.
    jsr wait_vblank

    ; Draw the input text field.
    ldx #INPUT_FIELD_X-1
    ldy #INPUT_FIELD_Y
    jsr goto_tile

    ; Draw a block character before the text.
    lda #$7f
    sta PPUDATA

    ldx #0

draw_input_text:
    lda INPUT_TEXT, x
    sta PPUDATA
    inx
    cpx #INPUT_TEXT_LENGTH
    bne draw_input_text

    ; Another block.
    lda #$7f
    sta PPUDATA

    ; Draw the cursor now.
    ldx #INPUT_FIELD_X
    ldy #INPUT_FIELD_Y+1
    jsr goto_tile
    ldx #0
draw_cursor:
    ; At the cursor?
    cpx CURSOR_POSITION
    beq cursor_character

    ; Nope! Draw a space.
    lda #$20
    sta PPUDATA
draw_cursor_next:
    ; Enough characters?
    inx
    cpx #INPUT_TEXT_LENGTH

    ; No, continue.
    bne draw_cursor
    jmp draw_cursor_done

    ; Branch for the cursor character.
cursor_character:
    ; Green asterisk.
    lda #$80
    sta PPUDATA
    jmp draw_cursor_next

    ; Done drawing.
draw_cursor_done:
    ; Reset the scrolling after writing into PPU registers.
    jsr reset_scroll

    ; Wait for joypad input.
wait_joypad:
    ; A delay to slow down and debounce.
    ldx #JOYPAD_DELAY
    jsr wait_vblanks

    ; Read.
    jsr read_joypad

    ; Test for keys.
    lda #JOYBTN_LEFT
    bit TEMP_A
    bne cursor_left
    lda #JOYBTN_RIGHT
    bit TEMP_A
    bne cursor_right
    lda #JOYBTN_UP
    bit TEMP_A
    bne character_inc
    lda #JOYBTN_DOWN
    bit TEMP_A
    bne character_dec
    lda #JOYBTN_START
    bit TEMP_A
    bne quit
    lda #JOYBTN_A
    bit TEMP_A
    bne a_upper
    lda #JOYBTN_B
    bit TEMP_A
    bne a_lower
    lda #JOYBTN_SELECT
    bit TEMP_A
    bne space

    ; None of the above, loop.
    jmp wait_joypad

; To avoid out-of-range branches.
input_loop_closer:
    jmp input_loop

; Move the cursor to the left.
cursor_left:
    ; Load the cursor position...
    lda CURSOR_POSITION

    ; Don't do anything if it's already zero.
    beq input_loop_closer

    ; Decrement it otherwise.
    dec CURSOR_POSITION

    ; Continue.
    jmp input_loop

; Move the cursor to the right.
cursor_right:
    ; Load the cursor position.
    lda CURSOR_POSITION

    ; Check if we can still increment it.
    cmp #INPUT_TEXT_LENGTH-1
    beq input_loop_closer

    ; Increment.
    inc CURSOR_POSITION

    ; Continue.
    jmp input_loop

; Increment the character if that's possible.
character_inc:
    ; Load the current character.
    ldx CURSOR_POSITION
    lda INPUT_TEXT, x

    ; If it's at the max value, do nothing.
    cmp #$7e
    beq input_loop_closer

    ; Increment and continue.
    inc INPUT_TEXT, x
    jmp input_loop

; Decrement.
character_dec:
    ; Load the current character.
    ldx CURSOR_POSITION
    lda INPUT_TEXT, x

    ; If it's at the min value, do nothing.
    cmp #$20
    beq input_loop_closer

    ; Decrement and continue.
    dec INPUT_TEXT, x
    jmp input_loop

; Upper-case A.
a_upper:
    ldx CURSOR_POSITION
    lda #"A"
    sta INPUT_TEXT, x
    jmp input_loop

; Lower-case A.
a_lower:
    ldx CURSOR_POSITION
    lda #"a"
    sta INPUT_TEXT, x
    jmp input_loop

; Space.
space:
    ldx CURSOR_POSITION
    lda #" "
    sta INPUT_TEXT, x
    jmp input_loop

    ; Quit from the input routine.
quit:
    ; Compute the actual length by finding the last non-space character.
    ldx #INPUT_TEXT_LENGTH-1
skip_whitespace:
    lda INPUT_TEXT, x
    cmp #$20
    bne done
    dex
    cpx #$ff
    bne skip_whitespace

    ; X is always length-1 even when it underflows to $ff.
    ; Incrementing it yields the valid value.
done:
    inx
    stx INPUT_LENGTH_STRIP
    rts
.)

;test_message:
; "BlueSyncLine!" pre-encoded
;.byt 64, 212, 38, 199, 86, 85, 55, 150, 230, 52, 198, 150, 230, 82, 16, 236, 17, 236, 17

test_text:
.byt "BlueSyncLine 2020"

intro_text:
.byt 128, "NES QR demo", 128, 0

directions_1:
.byt 128, "Move cursor....Left/Right", 0
directions_2:
.byt 128, "Change char....Up/Down", 0
directions_3:
.byt 128, "Shortcuts......A/B/SELECT", 0
directions_4:
.byt 128, "Finish.........START", 0
directions_5:
.byt 128, "BlueSyncLine; Public Domain", 0
quit_message:
.byt "Press any key to quit.", 0
